package utility;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Wait {

    /**
     * Click the element once it is ready
     * 
     * @param locator
     * @param timeout
     * @param driver
     */
    public static void clickWhenReady(WebElement locator, int timeout) {
        WebElement element = whenReady(locator, timeout);
        element.click();
    }

    /////////////////////////
    /////////////////////////////
    public static WebElement whenReady(WebElement locator, int timeout) {
        WebElement element = null;
        WebDriverWait wait = new WebDriverWait(DriverSingleton.getDriver(), timeout);
        return element = wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    public static void clickWhenVisible(WebDriver driver, By locator, int timeout) {
        WebElement element = null;
        WebDriverWait wait = new WebDriverWait(driver, timeout);
        element = wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        element.click();

    }

    public static void clickWhenClickable(WebElement webElement, int timeout) {
        WebElement element = null;
        WebDriverWait wait = new WebDriverWait(DriverSingleton.getDriver(), timeout);
        element = wait.until(ExpectedConditions.elementToBeClickable(webElement));
        element.click();

    }

    public static void waitForAjax() {
        new WebDriverWait(DriverSingleton.getDriver(), 10).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                JavascriptExecutor js = (JavascriptExecutor) driver;
                return (Boolean) js.executeScript("return jQuery.active == 0");
            }
        });

    }


    public static void waitForElementLocatorIsPresent(By locator, int timeout) {
        WebElement element = null;
        WebDriverWait wait = new WebDriverWait(DriverSingleton.getDriver(), timeout);

        element = wait.until(ExpectedConditions.presenceOfElementLocated(locator));

    }

    public static void waitUntilElementIsClickable(WebElement element, int timeout) {

        WebDriverWait wait = new WebDriverWait(DriverSingleton.getDriver(), timeout);

        element = wait.until(ExpectedConditions.elementToBeClickable(element));

    }



    public static void waitForVisibilityOfElement(WebElement element, Integer... timeOutInSeconds) {
        int attempts = 0;
        while (attempts < 3) {
            try {
                waitFor(ExpectedConditions.visibilityOf(element),
                        (timeOutInSeconds.length > 0 ? timeOutInSeconds[0] : null));
                break;
            } catch (StaleElementReferenceException e) {

            }
            attempts++;
        }

    }

    private static void waitFor(ExpectedCondition<WebElement> condition, Integer timeOutInSeconds) {

        timeOutInSeconds = timeOutInSeconds != null ? timeOutInSeconds : 10;
        WebDriverWait wait = new WebDriverWait(DriverSingleton.getDriver(), timeOutInSeconds);
        wait.until(condition);

    }

}
