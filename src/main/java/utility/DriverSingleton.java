package utility;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.events.EventFiringWebDriver;

public class DriverSingleton {
    private static EventFiringWebDriver webDriver;
    private static int implicitTimeoutDefaultValue = 10;

    // private constructor to prevent others from instantiating this class
    private DriverSingleton() {

    }

    // creates the instance in the global access method
    // synchronized -> parallel tests
    // Thread Safe Singleton :
    // https://www.journaldev.com/1377/java-singleton-design-pattern-best-practices-examples#thread-safe-singleton
    public static synchronized EventFiringWebDriver getDriver() {

        if (webDriver == null) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--no-sandbox");
            options.addArguments("--disable-dev-shm-usage");
            options.addArguments("--disable-extensions");
            System. setProperty("webdriver.chrome.driver", "src" + File.separator + "main" + File.separator + "resources" + File.separator +"chromedriver.exe");
            WebDriver driver = new ChromeDriver(options);
            // To enable logs in console
            webDriver = new EventFiringWebDriver(driver);
            webDriver.manage().timeouts().implicitlyWait(implicitTimeoutDefaultValue, TimeUnit.SECONDS);
            
            webDriver.manage().window().fullscreen();
            HandleEvents eventListener = new HandleEvents();
            DriverSingleton.getDriver().register(eventListener);
        }
        return webDriver;

    }

}
