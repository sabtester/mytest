package utility;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.UnhandledAlertException;

import com.relevantcodes.extentreports.LogStatus;

public class Screenshot {

    public static void takeScreenshotWithDetails(String details) {
        String fileName = "Screenshot" + System.currentTimeMillis() + ".png";
        String directory = "./TestReport/";
        try {
            File sourceFile = ((TakesScreenshot) DriverSingleton.getDriver()).getScreenshotAs(OutputType.FILE);
            String destination = directory + fileName;
            FileUtils.copyFile(sourceFile, new File(destination));
        } catch (UnhandledAlertException e) {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            Rectangle screenRectangle = new Rectangle(screenSize);

            try {
                Robot robot = new Robot();
                BufferedImage image = robot.createScreenCapture(screenRectangle);
                ImageIO.write(image, "png", new File(directory, fileName));
            } catch (AWTException | IOException ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        String imagepath = ExtentTestManager.getTest().addScreenCapture(fileName);

        if (Objects.nonNull(details)) {
            imagepath = details + ' ' + imagepath;
        }

        ExtentTestManager.getTest().log(LogStatus.INFO, "Screenshot", imagepath);
    }

    public static void takeScreenshot() {
        takeScreenshotWithDetails(null);
    }

    public static String takeScreenshot(String fileName) throws IOException, Exception {
        fileName = "Screenshot" + System.currentTimeMillis() + ".png";
        String directory = "./TestReport/";
        //String directory = System.getProperty("user.dir") +"/test-output/";
        try {
            File sourceFile = ((TakesScreenshot) DriverSingleton.getDriver()).getScreenshotAs(OutputType.FILE);
            String destination = directory + fileName;
            FileUtils.copyFile(sourceFile, new File(destination));

        } catch (UnhandledAlertException e) {
            Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
            Rectangle screenRectangle = new Rectangle(screenSize);
            Robot robot = new Robot();
            BufferedImage image = robot.createScreenCapture(screenRectangle);
            ImageIO.write(image, "png", new File(directory, fileName));
            // return destination;
        }
        return fileName;
    }

    public static void takeScreenshotOfPage() {
        String path;
        try {
            path = Screenshot.takeScreenshot("Screenshot");
            String imagepath = ExtentTestManager.getTest().addScreenCapture(path);
            ExtentTestManager.getTest().log(LogStatus.INFO, "Screenshot", imagepath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
