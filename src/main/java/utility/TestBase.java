package utility;

import java.lang.reflect.Method;

import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public abstract class TestBase {

    //public SystemProperty systemProperty;
    protected ExtentManager report;
    protected ExtentTest test;

    @BeforeTest
    public void setup() {
        DriverSingleton.getDriver().get("https://www.facebook.com/");

        initFunctions();

    }

    @BeforeSuite()
    public void beforeSuite() {

    }

    @Parameters({ "language", "jsonData", "property" })
    @BeforeClass()
    public void loadData(String language, String jsonData, String property) throws Exception {

       // systemProperty = PropertyReader.getSystemProperty(property);

        TestCaseData data = new TestCaseData(jsonData, language);

    }

    @AfterClass
    public void afterClass() {
        ExtentTestManager.getTest().log(LogStatus.INFO, "" + " ################Test has Ended###################");

    }

    public abstract void initFunctions();

    @AfterSuite
    public void tearDown() throws Exception {

        // DriverSingleton.getDriver().close();
        // DriverSingleton.getDriver().quit();

        ExtentManager.getReporter().flush();
        ExtentManager.getReporter().close();

    }

    @AfterTest
    public void afterTest() {

        ExtentTestManager.getTest().log(LogStatus.INFO, "" + "xxxxxxxxxxxxxxxxxTestcases have Endedxxxxxxxxxxxxxxxxxx");
    }

    @BeforeMethod
    public void logStartMethod(Method meth) {
        Test testAnnot = meth.getAnnotation(Test.class);
        String name = meth.getName();
        if (testAnnot != null) {
            String testDescription = testAnnot.description();
            if (testDescription != null) {
                name = testDescription;
            }
        }
        ExtentTestManager.startTest(name);
    }

    @AfterMethod
    public void logTestResultWithScreenshot(ITestResult testResult) throws Exception {

        if (testResult.getStatus() == ITestResult.FAILURE) {
            String path = Screenshot.takeScreenshot(testResult.getName());
            String imagepath = ExtentTestManager.getTest().addScreenCapture(path);
            ExtentTestManager.getTest().log(LogStatus.FAIL, testResult.getThrowable());
            ExtentTestManager.getTest().log(LogStatus.FAIL, testResult.getName(), imagepath);

        } else if (testResult.getStatus() == ITestResult.SUCCESS) {
            String path = Screenshot.takeScreenshot(testResult.getName());
            String imagepath = ExtentTestManager.getTest().addScreenCapture(path);

            ExtentTestManager.getTest().log(LogStatus.PASS, testResult.getName(), imagepath);

        } else if (testResult.getStatus() == ITestResult.SKIP) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "Test " + testResult.getName() + " Skipped");

        }
        ExtentManager.getReporter().endTest(ExtentTestManager.getTest());

    }

}
