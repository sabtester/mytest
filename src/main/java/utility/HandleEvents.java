package utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.AbstractWebDriverEventListener;

public class HandleEvents extends AbstractWebDriverEventListener {

    public void afterClickOn(WebElement arg0, WebDriver arg1) {
      
     }

    public void afterNavigateTo(String arg0, WebDriver driver) {
        
    }

    public void beforeClickOn(WebElement element,  WebDriver driver) {
        Log.info("Clicking on " + element.toString());
    }

    public void beforeNavigateTo(String url, WebDriver driver) {
      

    }

    public void onException(Throwable error, WebDriver driver) {

   }
}