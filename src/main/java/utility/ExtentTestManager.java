package utility;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import java.util.HashMap;
import java.util.Map;

public class ExtentTestManager {
    private static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();
    private static ExtentReports extent = ExtentManager.getReporter();

    public static synchronized ExtentTest getTest() {
        return extentTestMap.get((int) Thread.currentThread().getId());
    }

    public static synchronized void endTest() {
        extent.endTest(extentTestMap.get((int) Thread.currentThread().getId()));
    }

    public static synchronized void startTest(String testName) {
        startTest(testName, "Test started");
    }

    public static synchronized void startTest(String testName, String description) {
        ExtentTest test = extent.startTest(testName, description);
        extentTestMap.put((int) Thread.currentThread().getId(), test);
    }
}
