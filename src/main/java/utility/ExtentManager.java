package utility;

import java.io.File;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentManager {

    private static ExtentReports extent;

    public synchronized static ExtentReports getReporter() {
        if (extent == null) {
            extent = new ExtentReports("./TestReport/Report.html", true);
            extent.addSystemInfo("Selenium Version", "3.12.0");
            extent.addSystemInfo("Author", "Test Team");
            extent.loadConfig(new File(System.getProperty("user.dir") + "/reportConfig/extent-config.xml"));
        }
        return extent;
    }

    public void logWithH4Heading(String message) {

        ExtentTestManager.getTest().log(LogStatus.INFO, "<h4>" + message + "</h4>");

    }

    public void logWithH3Heading(String message) {

        ExtentTestManager.getTest().log(LogStatus.INFO, "<h3>" + message + "</h3>");

    }

    public void logWithH5Heading(String message) {

        ExtentTestManager.getTest().log(LogStatus.INFO, "<h5>" + message + "</h4>");

    }
}
