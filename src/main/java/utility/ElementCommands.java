package utility;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.LogStatus;

public class ElementCommands {

    public static void verifySelectedOption(WebElement element, JsonObject data, String jsonObjectKey) {
        Select select = new Select(element);
        WebElement option = select.getFirstSelectedOption();
        String actualText = option.getText();
        String expectedText = data.get(jsonObjectKey).getAsString();
        ExtentTestManager.getTest().log(LogStatus.INFO,
                "Actual Text from page is : " + actualText.trim() + ", Expected Value is : " + expectedText);
        Assert.assertEquals(actualText, expectedText, "Wrong Option is selected");
    }

    public static void clickOn(WebElement element) {
        if (element != null) {
            ExtentTestManager.getTest().log(LogStatus.INFO, "Clicking on " + element.getText().trim() + " Link ");
            // Wait.waitTime(SystemProperty.getWaitShort());
            element.click();
        } else {
            ExtentTestManager.getTest().log(LogStatus.INFO, "WebElement not exist");
        }
    }

    public static void enterElementText(WebElement element, String text) {
        Wait.waitForVisibilityOfElement(element, 20);
        Wait.whenReady(element, 5);
        ExtentTestManager.getTest().log(LogStatus.INFO, "Entering  " + text);
        element.sendKeys(text);
        Wait.waitForAjax();
    }

    /**
     * @param element
     *            the webElement
     * @param data
     *            json object
     * @param jsonObjectKey
     *            is the key from json is the
     */
    public static void enterElementText(WebElement element, JsonObject data, String jsonObjectKey) {
        if (data.get(jsonObjectKey) != null) {
            Wait.waitForVisibilityOfElement(element, 10);
            Wait.whenReady(element, 5);
            ExtentTestManager.getTest().log(LogStatus.INFO, "Entering  " + data.get(jsonObjectKey).getAsString());
            element.clear();
            element.sendKeys(data.get(jsonObjectKey).getAsString());
        } else {
            ExtentTestManager.getTest().log(LogStatus.INFO, "WebElement not exist");
        }
    }

    public static String getElementTextSimple(WebElement element) {
        ExtentTestManager.getTest().log(LogStatus.INFO, "The text in the element is:  " + element.getText());
        return element.getText();
    }

    public static void getElementText(WebElement element) {
        Wait.waitForVisibilityOfElement(element, 10);
        Wait.whenReady(element, 5);
        ExtentTestManager.getTest().log(LogStatus.INFO, "Coping " + element.getText().trim() + "Text from page");
        element.getText();
        Wait.waitForAjax();
    }

    public static void selectOptionByVisibleText(WebElement element, JsonObject data, String jsonObjectKey) {
        if (element != null) {
            Wait.waitForVisibilityOfElement(element, 10);
            Wait.whenReady(element, 5);
            ExtentTestManager.getTest().log(LogStatus.INFO,
                    "Selecting " + data.get(jsonObjectKey).getAsString() + " from Dropdown menu");
            Select propertyType = new Select(element);
            propertyType.selectByVisibleText((data.get(jsonObjectKey).getAsString()));
        } else {
            ExtentTestManager.getTest().log(LogStatus.INFO, "WebElement not exist");
        }
    }

    public static void confirmRadioSelectionOnOrOff(WebElement element, boolean selectedOrNot) {

        // if true should be selected
        if (selectedOrNot) {
            assertTrue(element.isSelected());
        }
        // if false confirm NOT selected
        else {
            assertFalse(element.isSelected());
        }

    }

    public static void verifyTextBoxAttribute(WebElement element, JsonObject data, String jsonObjectKey) {
        Wait.waitForVisibilityOfElement(element, 10);
        Wait.whenReady(element, 5);
        String expectedText = data.get(jsonObjectKey).getAsString();
        String actualText = element.getAttribute("value");
        ExtentTestManager.getTest().log(LogStatus.INFO,
                "Actual Text from page is : " + actualText.trim() + ", Expected Value is : " + expectedText);
        Assert.assertEquals(actualText, expectedText, "Wrong or Empty value or text");
    }

    public static void verifyErrorMessage(WebElement element, JsonObject data, String jsonObjectKey) {
        String actual = ElementCommands.getElementTextSimple(element);
        String expectedText = data.get(jsonObjectKey).getAsString();
        ExtentTestManager.getTest().log(LogStatus.INFO,
                "Actual Text from page is : " + actual.trim() + ", Expected Value is : " + expectedText);
        Assert.assertEquals(actual, expectedText, "Wrong or no Error message");

    }

}
