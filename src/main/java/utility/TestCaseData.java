package utility;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class TestCaseData {

    private static final String JSON_DATA_LOCATION = "src" + File.separator + "test" + File.separator + "resources"
            + File.separator + "jsondata" + File.separator + "%s" + File.separator + "%s.json";

    private String dataFile;
    static JsonObject dataSet = null;

    public TestCaseData(String testDataName, String locale) {
        this.dataFile = String.format(JSON_DATA_LOCATION, locale, testDataName);
        dataSet = parseJsonFile(this.dataFile);
    }

    public static Object[][] readJsonTestDataAsJsonObject(String testname) {
        Object[][] obj;
        JsonElement testData = dataSet.get(testname);

        if (testData.isJsonArray()) {
            JsonArray testDataArr = testData.getAsJsonArray();

            obj = new Object[testDataArr.size()][1];
            for (int i = 0; i < testDataArr.size(); i++) {
                JsonObject ele = testDataArr.get(i).getAsJsonObject();
                obj[i][0] = ele;
            }
        } else {
            JsonObject data = testData.getAsJsonObject();
            obj = new Object[1][1];
            obj[0][0] = data;
        }

        return obj;
    }

    private static JsonObject parseJsonFile(String filename) {
        // Read from File to String
        JsonObject jsonObject = new JsonObject();

        try {
            JsonParser parser = new JsonParser();

            File jsonFile = new File(filename);
            JsonElement jsonElement = parser.parse(new FileReader(jsonFile));
            jsonObject = jsonElement.getAsJsonObject();
            return jsonObject;
        } catch (Exception e) {
            System.out.println("Error parsing JSON file");
            return null;
        }
    }
}
