package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginOrSignUpPageUIElements {
    @FindBy(name = "email")
    protected WebElement emailOrPhoneTxtbx;

    @FindBy(name = "pass")
    protected WebElement passwordTxtbx;

    @FindBy(id="loginbutton")
    protected WebElement loginBtn;
    @FindBy(name = "firstname")
    protected WebElement firstNameTxtbx;

    @FindBy(name = "lastname")
    protected WebElement surnameTxtbx;

    @FindBy(name = "reg_email__")
    protected WebElement mobileOrEmailTxtbx;

    @FindBy(name = "reg_passwd__")
    protected WebElement newPasswordTxtbx;

    @FindBy(id = "day")
    protected WebElement dayDrpdwn;

    @FindBy(id = "month")
    protected WebElement monthDrpdwn;

    @FindBy(id = "year")
    protected WebElement yearDrpwn;

    @FindBy(css = "input[type='radio'][value='1']")
    protected WebElement femaleRdbtn;
    @FindBy(css = "input[type='radio'][value='2']")
    protected WebElement maleRdBtn;

    @FindBy(id = "u_0_b")
    protected WebElement customRdBtn;

    @FindBy(id = "u_0_17")
    protected WebElement signUpBtn;

}
