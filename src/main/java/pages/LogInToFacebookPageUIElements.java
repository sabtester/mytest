package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LogInToFacebookPageUIElements {
    
    @FindBy(name = "email")
    protected WebElement emailOrPhoneTxtbx;

    @FindBy(name = "pass")
    protected WebElement passwordTxtbx;

    @FindBy(id="loginbutton")
    protected WebElement loginBtn;
    @FindBy(name = "firstname")
    protected WebElement forgottenPasswordLnk;

    @FindBy(xpath="//*[@id='globalContainer']/div[3]/div/div/div")
    protected WebElement errorMessageTxt;

}
