package pages;

import org.openqa.selenium.support.PageFactory;

import com.google.gson.JsonObject;

import utility.DriverSingleton;
import utility.ElementCommands;

public class LogInToFacebookPageFuntions extends LogInToFacebookPageUIElements {

    protected LogInToFacebookPageUIElements loginToFacebookUIElements;

    public LogInToFacebookPageFuntions() {
        super();
        loginToFacebookUIElements = PageFactory.initElements(DriverSingleton.getDriver(),
                LogInToFacebookPageUIElements.class);
    }

    //Assert Error Message because wrong email and password were populated
    public void verifyAlertMessage(JsonObject data) {
        ElementCommands.verifyErrorMessage(loginToFacebookUIElements.errorMessageTxt, data, "errorMessage");
    }

}
