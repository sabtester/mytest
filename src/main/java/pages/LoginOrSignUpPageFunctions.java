package pages;

import org.openqa.selenium.support.PageFactory;

import com.google.gson.JsonObject;
import com.relevantcodes.extentreports.LogStatus;

import utility.DriverSingleton;
import utility.ElementCommands;
import utility.ExtentTestManager;

public class LoginOrSignUpPageFunctions extends LoginOrSignUpPageUIElements {

    protected LoginOrSignUpPageUIElements createAccountUIElements;

    public LoginOrSignUpPageFunctions() {
        super();
        createAccountUIElements = PageFactory.initElements(DriverSingleton.getDriver(), LoginOrSignUpPageUIElements.class);

    }
    
    public void login(JsonObject data){
        ElementCommands.enterElementText(createAccountUIElements.emailOrPhoneTxtbx, data, "email");
        ElementCommands.enterElementText(createAccountUIElements.passwordTxtbx, data, "password");
        ElementCommands.clickOn(createAccountUIElements.loginBtn);

    }

    public void createFacebookAccount(JsonObject data) {
        ElementCommands.enterElementText(createAccountUIElements.firstNameTxtbx, data, "firstName");
        ElementCommands.enterElementText(createAccountUIElements.surnameTxtbx, data, "lastName");
        ElementCommands.enterElementText(createAccountUIElements.mobileOrEmailTxtbx, data, "mobile");
        ElementCommands.enterElementText(createAccountUIElements.newPasswordTxtbx, data, "password");
        ElementCommands.selectOptionByVisibleText(createAccountUIElements.dayDrpdwn, data, "day");
        ElementCommands.selectOptionByVisibleText(createAccountUIElements.monthDrpdwn, data, "month");
        ElementCommands.selectOptionByVisibleText(createAccountUIElements.yearDrpwn, data, "year");
        ElementCommands.clickOn(createAccountUIElements.maleRdBtn);
        //@@@@@@@@@@@@@ Didn't sign in on purpose @@@@@@@@@@@
        //ElementCommands.clickOn(createAccountUIElements.signUpBtn);
        
        //Assert all populated fields
        ElementCommands.verifyTextBoxAttribute(createAccountUIElements.firstNameTxtbx, data, "firstName");
        ElementCommands.verifyTextBoxAttribute(createAccountUIElements.surnameTxtbx, data, "lastName");
        ElementCommands.verifyTextBoxAttribute(createAccountUIElements.mobileOrEmailTxtbx, data, "mobile");
        ElementCommands.verifyTextBoxAttribute(createAccountUIElements.newPasswordTxtbx, data, "password");
        ElementCommands.verifySelectedOption(createAccountUIElements.dayDrpdwn, data, "day");
        ElementCommands.verifySelectedOption(createAccountUIElements.monthDrpdwn, data, "month");
        ElementCommands.verifySelectedOption(createAccountUIElements.yearDrpwn, data, "year");
        
    }

}
