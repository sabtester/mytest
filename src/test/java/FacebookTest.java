
import java.io.IOException;

import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.google.gson.JsonObject;

import pages.LogInToFacebookPageFuntions;
import pages.LoginOrSignUpPageFunctions;
import utility.DriverSingleton;
import utility.ExtentManager;
import utility.TestBase;
import utility.TestCaseData;

public class FacebookTest extends TestBase {

    // declaring the classes
    private LoginOrSignUpPageFunctions facebook;
    private LogInToFacebookPageFuntions errorMassage;
    private TestCaseData data;

    ExtentManager extent = new ExtentManager();

    @Override
    public void initFunctions() {
        // initialising all the classes with page factory
        facebook = PageFactory.initElements(DriverSingleton.getDriver(), LoginOrSignUpPageFunctions.class);
        errorMassage = PageFactory.initElements(DriverSingleton.getDriver(), LogInToFacebookPageFuntions.class);
    }

    @DataProvider

    public Object[][] createAccountData() throws IOException {
        return TestCaseData.readJsonTestDataAsJsonObject("CreateAccountData");
    }

    @Test(dataProvider = "createAccountData", description = "Creating new Facebook Account")
    public void createNewAccountTest(JsonObject data) throws Exception {

        extent.logWithH3Heading("Itteration: " + data.get("description").getAsString());

        facebook.createFacebookAccount(data);

    }

    @DataProvider

    public Object[][] wrongPasswordData() throws IOException {
        return TestCaseData.readJsonTestDataAsJsonObject("LoginWrongPasswordData");
    }

    @Test(dataProvider = "wrongPasswordData", description = "Login to Facebook with wrong password and verify error message")
    public void loginWrongPaswordTest(JsonObject data) throws Exception {

        facebook.login(data);
        errorMassage.verifyAlertMessage(data);

    }
}
